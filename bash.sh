#!/bin/bash

date > index.html
{
    docker run -i --name karina -v `pwd`:/usr/src/app -w /usr/src/app node:alpine sh -c "npm install && npm run lint && npm test" &&
    echo '<pre>' >> index.html
    docker run --rm grycap/cowsay /usr/games/cowsay "Every thingth is ok!" >> index.html
    echo '</pre>' >> index.html
} || {
    docker logs karina 2>&1 | tee index.html
}
docker rm karina
touch Dockerfile
echo 'FROM nginx2COPY . /usr/share/nginx/html' | sed -e 's/2/\n/g' > Dockerfile
docker login registry.gitlab.com
docker build -t registry.gitlab.com/karina_oliinyk/array_test_cases .
docker push registry.gitlab.com/karina_oliinyk/array_test_cases

ssh Karina@104.248.138.53 'sh /home/Karina/start_test_gitlab.sh'
