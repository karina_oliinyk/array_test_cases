function MyArray(arg, ...args) {
  let { length } = arguments;

  if (!new.target) {
    return new MyArray(arg, ...args);
  }

  if (length === 1 && (arg < 0 || arg > 2 ** 32 - 1 || Number.isNaN(arg))) {
    throw new RangeError('Invalid array length');
  }

  if (length === 1 && typeof arg === 'number') {
    this.length = arg;
    return this;
  }

  for (let i = 0; i < length; i++) {
    this[i] = arguments[i];
  }

  Object.defineProperty(this, 'length', {
    enumerable: false,
    set(newLength) {
      if (typeof newLength !== 'number') {
        throw new RangeError('Invalid array length');
      }

      if (length > newLength) {
        for (let i = length - 1; i >= newLength; i--) {
          delete this[i];
        }
      }

      length = newLength;
    },
    get() {
      return length;
    }
  });

  const proxy = new Proxy(this, {
    get(obj, prop) {
      return obj[prop];
    },
    set(obj, prop, value) {
      obj[prop] = value;

      if (!(prop === 'length')) {
        const newLength = Number(prop) ? Math.abs(Number(prop)) : obj.length;

        if (obj.length < newLength) {
          obj.length = newLength + 1;
        }
      }
    }
  });

  return proxy;
}

MyArray.from = function(arrLike, ...args) {
  const instance = new MyArray();
  const mapFn = args[0];
  const thisArg = args[1];

  for (let i = 0; i < arrLike.length; i++) {
    instance[i] = arrLike[i];

    if (mapFn && thisArg) {
      instance[i] = mapFn.call(thisArg, arrLike[i]);
    } else if (mapFn) {
      instance[i] = mapFn(arrLike[i]);
    }
  }

  if (typeof arrLike === 'number') {
    instance.length = 0;
  }

  if (arrLike.length) {
    instance.length = arrLike.length;
  }

  return instance;
};

MyArray.prototype.push = function(arg, ...args) {
  if (!this.length || typeof this.length !== 'number') {
    this.length = 0;
  }

  for (let i = 0; i < arguments.length; i++) {
    this[this.length] = arguments[i];
    this.length += 1;
  }
  return this;
};

MyArray.prototype.pop = function() {
  if (this.length === 0) {
    return undefined;
  }

  if (!this.length || typeof this.length !== 'number') {
    this.length = 0;
    return this.length;
  }

  const lastArg = this[this.length - 1];
  delete this[this.length - 1];
  this.length -= 1;
  return lastArg;
};

MyArray.prototype.map = function(cb, ...args) {
  const instance = new MyArray();
  const thisArg = args[0];
  const thisCb = cb.bind(thisArg);
  const { length } = this;

  for (let i = 0; i < length; i++) {
    instance.length += 1;

    if (i in this) {
      instance[i] = thisCb(this[i], i, this);
    }
  }
  return instance;
};

MyArray.prototype.forEach = function(cb, thisArg = this) {
  const thisCb = cb.bind(thisArg);
  const { length } = this;

  for (let i = 0; i < length; i++) {
    if (i in this) {
      thisCb(this[i], i, this);
    }
  }
};

MyArray.prototype.reduce = function(cb) {
  const initValue = arguments[1];
  const firstVal = arguments.length > 1 ? 0 : 1;
  const { length } = this;
  let accumulator = this[0];

  if (arguments.length > 1) {
    accumulator = arguments[1];
  }

  if (!length && !initValue) {
    throw new TypeError();
  }

  if (!length && !initValue) {
    throw new TypeError();
  }

  for (let i = firstVal; i < length; i++) {
    if (i in this) {
      accumulator = cb(accumulator, this[i], i, this);
    }
  }

  return accumulator;
};

MyArray.prototype.filter = function(cb) {
  const instance = new MyArray();
  const thisArg = arguments[1];
  const curLength = this.length;

  for (let i = 0; i < curLength; i++) {
    const value = this[i];

    if (value && cb.call(thisArg, value, i, this)) {
      instance.push(value);
    }
  }

  return instance;
};

MyArray.prototype.sort = function(cb) {
  const { length } = this;
  const countCallback = new MyArray();

  if (!cb) {
    for (let j = this.length - 1; j > 0; j--) {
      for (let i = 0; i < j; i++) {
        if (String(this[i]).charCodeAt() > String(this[i + 1]).charCodeAt()) {
          const interim = this[i];
          this[i] = this[i + 1];
          this[i + 1] = interim;
        }
      }
    }
    return this;
  }

  for (let i = 1; i < length; i++) {
    if (this[i]) {
      countCallback.push(cb(this[i], this[i - 1]));
    }
  }

  for (let i = 0; i < countCallback.length; i++) {
    if (!cb.length && countCallback[i] === -1) {
      const copyThis = new MyArray(...this);
      let index = 0;

      for (let i = copyThis.length - 1; i >= 0; i--) {
        this[index] = copyThis[i];
        index += 1;
      }
      return this;
    }
  }

  if (countCallback[0] > 0) {
    for (let j = this.length - 1; j > 0; j--) {
      for (let i = 0; i < j; i++) {
        if (this[i] > this[i + 1]) {
          const interim = this[i];
          this[i] = this[i + 1];
          this[i + 1] = interim;
        }
      }
    }
    return this;
  }

  if (countCallback[0] < 0) {
    for (let j = this.length - 1; j > 0; j--) {
      for (let i = 0; i < j; i++) {
        if (this[i] < this[i + 1]) {
          const interim = this[i];
          this[i] = this[i + 1];
          this[i + 1] = interim;
        }
      }
    }
    return this;
  }
};


MyArray.prototype.toString = function() {
  let string = '';

  if (this.length === 0) {
    return string;
  }

  for (let i = 0; i < this.length; i++) {
    string += `${this[i] || ''},`;
  }

  const result = string.slice(0, -1);

  return result;
};

MyArray.prototype[Symbol.iterator] = function() {
  const itThis = this;

  return {
    current: 0,
    last: this.length - 1,
    next() {
      if (this.current <= this.last) {
        const currentInc = this.current;
        this.current += 1;
        return { done: false, value: itThis[currentInc] };
      } else {
        return { done: true };
      }
    }
  };
};

MyArray.prototype.indexOf = function(searchEl) {
  const fromIndex = arguments[1];

  if (fromIndex < 0 || fromIndex >= this.length) {
    return -1;
  }

  for (let i = 0; i < this.length; i++) {
    if (this[i] === searchEl) {
      return i;
    }
  }

  return -1;
};

MyArray.prototype.shift = function() {
  const shifted = this[0];
  delete this[0];

  for (let i = 0; i < this.length; i++) {
    this[i] = this[i + 1];
  }

  this.length -= 1;

  if (!this.length) {
    this.length = 0;
  }

  return shifted;
};

MyArray.prototype.unshift = function(arg, ...args) {
  const instance = new MyArray(arg, ...args);

  if (isNaN(this.length) || this.length === undefined) {
    this.length = 0;
    return this.length;
  }

  this.length = this.length + args.length + 1;

  for (let i = this.length - 1; i >= 0; i--) {
    this[i + args.length + 1] = this[i];
    delete this[i];

    if (i < args.length + 1) {
      this[i] = instance[i];
    }
  }

  return this.length;
};

MyArray.prototype.slice = function(begin, end) {
  let { length } = this;
  let startEl = Number(begin) || 0;
  let endEl = typeof end !== 'undefined' ? Number(end) : length;

  if (isNaN(endEl) || begin > length) {
    return new MyArray();
  }

  if (Number(end) < 0) {
    endEl = end + length;
  }

  startEl = startEl >= 0 ? startEl : length + startEl;

  let endLength = endEl - startEl;
  endLength = endLength > length ? length : endLength;

  const newArr = new MyArray(endLength);

  for (let i = 0; i < endLength; i++) {
    newArr[i] = this[startEl + i];
    length += 1;
  }

  return newArr;
};

MyArray.prototype.splice = function(start, deleteCount, ...args) {
  const instance = new MyArray();
  let startEl = Math.floor(start);
  let delEl = deleteCount;
  let counter = 0;

  if (delEl === undefined) {
    delEl = this.length;
  }

  if (delEl >= (this.length - startEl)) {
    delEl = this.length - startEl;
  }

  if (startEl < 0) {
    startEl += this.length;
  }

  if (isNaN(startEl) && isNaN(delEl)) {
    return { ...this };
  }

  if (isNaN(startEl)) {
    startEl = 0;
    delEl = this.length;
  }

  if (startEl === -Infinity) {
    startEl = 0;
  }

  const newLength = startEl + delEl;

  if (args.length === 0) {
    for (let i = startEl; i < newLength; i++) {
      instance[counter] = this[i];
      this[i] = this[i + delEl];
      counter += 1;
    }
    this.length -= delEl;
  }

  if (args.length > 0 && delEl === 0) {
    for (let i = startEl; i < args.length + 1; i++) {
      this[i + 1] = this[i];
      this[i] = args[counter];
      counter += 1;
      this.length += 1;
    }
    this.length -= delEl;
  }


  if (args.length > 0) {
    for (let i = startEl; i < newLength; i++) {
      instance[counter] = this[i];
      this[i] = args[counter];
      counter += 1;
    }
    this.length -= delEl;
  }

  instance.length = counter;

  return instance;
};

const arr = new MyArray(1, 4, [2, 4], { name: 'Jack' });

module.exports = MyArray;
